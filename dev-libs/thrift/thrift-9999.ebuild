# Distributed under the terms of the GNU General Public License v2

EAPI=6

if [[ ${PV} = 9999* ]]; then
	inherit git-r3
	# EGIT_BRANCH="python3"
	EGIT_MIN_CLONE_TYPE="single"
	EGIT_REPO_URI="https://github.com/apache/thrift.git"
else
	SRC_URI="mirror://apache/${PN}/${PV}/${P}.tar.gz"
	KEYWORDS="~x86 ~amd64"
fi

PYTHON_COMPAT=( python{3_1,3_2,3_3,3_4,3_5} )
GENTOO_DEPEND_ON_PERL="no"

inherit eutils flag-o-matic autotools distutils-r1 perl-module

DESCRIPTION="Lightweight, language-independent software stack with associated code generation mechanism for RPC"
HOMEPAGE="http://thrift.apache.org"

LICENSE="Apache-2.0"
SLOT="0"
IUSE="+pic +cpp c_glib csharp java erlang python perl php php_extension ruby haskell go nodejs qt4 qt5 static static-libs test"


#FIXME: java.eclassesnotused          1
RDEPEND=">=dev-libs/boost-1.40.0
	virtual/yacc
	dev-libs/openssl:=
	cpp? (
		>=sys-libs/zlib-1.2.3
		dev-libs/libevent
	)
	csharp? ( >=dev-lang/mono-1.2.4 )
	java? (
		dev-java/ant-ivy:=
		dev-java/commons-lang:=
		dev-java/slf4j-api:=
	)
	erlang? ( >=dev-lang/erlang-12.0.0 )
	python? (
		>=dev-lang/python-2.7
		dev-python/six
		dev-python/backports-ssl-match-hostname
		dev-python/ipaddress
		!dev-python/thrift
	)
	perl? (
		dev-lang/perl
		dev-perl/Bit-Vector
		dev-perl/Class-Accessor
	)
	php? ( dev-lang/php:= )
	php_extension? ( dev-lang/php:= )
	ruby? ( virtual/rubygems )
	haskell? ( dev-lang/ghc )
	go? ( sys-devel/gcc:=[go] )
	nodejs? ( net-libs/nodejs )
	qt4? ( dev-qt/qtcore:4 )
	qt5? ( dev-qt/qtcore:5 )
	"

REQUIRED_USE="^^ ( qt4 qt5 )"

DEPEND="${RDEPEND}
	>=sys-devel/gcc-4.2[cxx]
	c_glib? ( dev-libs/glib )
	sys-devel/flex
	python? ( dev-python/setuptools[${PYTHON_USEDEP}] )
	virtual/pkgconfig
	"

src_prepare() {
	# bootstrap source
	cd ${S} && ./bootstrap.sh
	default

	if use python ; then
		cd "${S}"/lib/py
		# Fix requirements.txt
		echo "" > requirements.txt
		distutils-r1_src_prepare
	fi
}

src_configure() {
	local myconf
	for USEFLAG in ${IUSE}; do
		myconf+=" $(use_with ${USEFLAG/+/})"
	done
	# This flags either result in compilation errors
	# or byzantine runtime behaviour.
	filter-flags -fwhole-program -fwhopr

	econf \
		--prefix="${EPREFIX}"/usr \
		--exec-prefix="${EPREFIX}"/usr \
		PY_PREFIX="${EPREFIX}"/usr \
		JAVA_PREFIX="${EPREFIX}"/usr/local/lib \
		PHP_PREFIX="${EPREFIX}"/usr/local/lib \
		PHP_CONFIG_PREFIX="${EPREFIX}"/etc/php.d \
		PERL_PREFIX="${EPREFIX}"/usr/local/lib \
		$(use_enable static-libs static) \
		$(use_enable test) \
		${myconf} \
		--without-{python,perl}

	if use perl ; then
		cd "${S}"/lib/perl
		perl-module_src_configure
	fi
}

src_compile() {
	default

	if use perl ; then
		cd "${S}"/lib/perl
		perl-module_src_compile
	fi

	if use python ; then
		cd "${S}"/lib/py
		distutils-r1_src_compile
	fi
}

src_install() {
	# default
	prune_libtool_files
	emake DESTDIR="${D}" install || die "emake install failed"

	if use perl ; then
		cd "${S}"/lib/perl
	perl-module_src_install
	fi

	if use python ; then
	cd "${S}"/lib/py
	distutils-r1_src_install
	fi
}
